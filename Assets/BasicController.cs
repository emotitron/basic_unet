﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using emotitron.Compression;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace emotitron.Compression.Sample
{


	/// <summary>
	/// A VERY basic player movement and position/rotation sync example using UNET.
	/// </summary>
	public class BasicController : MonoBehaviour
	{
		NetworkIdentity ni;


		public float movespeed = 2f;
		public float turnspeed = 30f;

		void Awake()
		{
			ni = GetComponent<NetworkIdentity>();
		}


		// Update is called once per frame
		void FixedUpdate()
		{
			if (!ni.isLocalPlayer)
				return;

			Vector3 pos = transform.position;

			if (Input.GetKey("w"))
				pos += transform.forward * movespeed * Time.deltaTime;

			if (Input.GetKey("s"))
				pos -= transform.forward * movespeed * Time.deltaTime;

			if (Input.GetKey("a"))
				pos -= transform.right * movespeed * Time.deltaTime;

			if (Input.GetKey("d"))
				pos += transform.right * movespeed * Time.deltaTime;

		
			transform.position = pos;


			Vector3 euler = transform.eulerAngles;

			if (Input.GetKey("q"))
				euler += new Vector3(0, turnspeed * Time.deltaTime, 0);

			if (Input.GetKey("e"))
				euler += new Vector3(0, -turnspeed * Time.deltaTime, 0);

			if (Input.GetKey("r"))
				euler += new Vector3(turnspeed * Time.deltaTime, 0, 0);

			if (Input.GetKey("c"))
				euler += new Vector3(-turnspeed * Time.deltaTime, 0, 0);

			if (Input.GetKey("1"))
				euler += new Vector3(0, 0, -turnspeed * Time.deltaTime);

			if (Input.GetKey("4"))
				euler += new Vector3(0, 0, turnspeed * Time.deltaTime);

			transform.eulerAngles = euler;
		}
	}

#if UNITY_EDITOR

	[CustomEditor(typeof(BasicController))]
	[CanEditMultipleObjects]

	public class BasicControllerEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			EditorGUILayout.HelpBox("This basic controller makes use of TransformCrusher.Clamp(), which allows it to restrict movement to the ranges of the crusher.", MessageType.None);
			base.OnInspectorGUI();
		}
	}

#endif
}

