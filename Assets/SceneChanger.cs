﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SceneChanger : MonoBehaviour {

	public KeyCode scene1Key = KeyCode.Alpha1;
	public KeyCode scene2Key = KeyCode.Alpha2;
	public KeyCode scene3Key = KeyCode.Alpha3;

	// Update is called once per frame
	void Update()
	{
		if (!NetworkServer.active)
			return;

		if (Input.GetKeyDown(scene1Key))
			NetworkManager.singleton.ServerChangeScene("Scene1");

		if (Input.GetKeyDown(scene2Key))
			NetworkManager.singleton.ServerChangeScene("Scene2");

		if (Input.GetKeyDown(scene3Key))
			NetworkManager.singleton.ServerChangeScene("Scene3");
	}
}
